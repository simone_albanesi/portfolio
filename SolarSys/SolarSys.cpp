#include <iostream>
#include <fstream>
#include <cmath>
#include <iomanip>
#include <ctime>
#include <cstdlib>
#define J 6         //index to use for jump  to one planet to another (6 coord, 3 for position and 3 for velocity) 
#define NUM 10      //sun + number of planets (we call them "planets" for brevity, but some of them are dwarf planets) 
#define NEQ (NUM*J) //number of equations
#define FIX_SUN 1   //if this is true(1), we fix the sun in (0,0) (we don't consider sun motion)

using namespace std;

static double mass[NEQ]; //global beacause we use this in RHS 

void RHS(double, double*, double*);

void forestRuth3DStep(double,double*, double, int, void (*)(double,double*,double*));
int bisection(double (*func)(double), double a, double b, double tol, double &zero); //ritorna 1 se trova uno zero (e lo salva in zero), 0 se non trova niente
static double g_E, g_M, g_e; //we need this in the function to find true anomaly
double eqEccentricityAnomaly(double); 

int main()
{
    double ipv[NEQ];//these are the initial conditions (0->5 components of sun, 6->11 mercury etc.) 
    double Y[NEQ];  //these are the position and velocity components that we update at every step 
    double in_pos[NUM], in_vel[NUM], orb_incl[NUM]; //data to use for creating ipv[] (for closed orbit: in_pos = perihelion, 
    //in_vel = v_min)
    double long_per[NUM]; //longitude of perihelion  
    double smajor_axis[NUM], ecc[NUM]; //ecc = orbital eccentricity
    double mean_anomaly[NUM], true_anomaly[NUM], mean_long[NUM], long_asc_node[NUM], eccentricity_anomaly[NUM];
    string names[NUM]; //planets' names 
    
    //Variables for counting the orbits of the planet that we use as reference. 
    double dt, t, dtheta; //parameters to use when we solve the ODE

    int p  = 3; //index for the planet to use as reference (i.e. mercury 1, venus 2 etc.) 
    int pY = p*J; //index for the planet that we use as reference to use in Y
    int p_orbits = 323; //the calculation stop when the p planet has done this number of orbits
    
    double vx1[NUM], vx2[NUM];  // we use these variables to see if we have a turning point
    int turning_points[NUM]; //number of turning(inversion) points
    double t_orb[NUM], flag_t_orb[NUM];
    
    for(int i=0; i<NUM; i++)
    {
        flag_t_orb[i] = 0;
        turning_points[i] = 0; 
    }

    dtheta = 1.e-3; //if too big the calculation fails, if too small it takes too much time 
    t = 0; //initial time 
        
    double l0,t0,m0; //costants for convertion to code-values
    double GM_sun = 13.26e19; //IS

    l0 = 1.49598e11; //UA
    t0 = sqrt(l0*l0*l0/GM_sun); 
    m0 = 5.9724e24; //earth's mass 

    int count_interval,interval; //we use these variables because we don't want to write every step on disk.
    //we don't need to write every step because: 1) otherwise the file .dat would be too big, gnuplot would crash 2) writing on disk
    //takes time 3) it makes no sense to write every step 
    count_interval = 0; 
    interval = (1/dtheta)/10;
    //note: if interval !=1, we are not sure to "draw" all the orbit 
    clock_t initial_clock;
    double last_t; 
    //+++++++++++++++++++++++++++++++++++++++ initialization of physical quantities ++++++++++++++++++++++++++++++++++++++++ 

    //data of the sun and the planets in IS (angles in degree)
    names[0] = "Sun";        mass[0] = 1.9885e30;     orb_incl[0] = 0;   
    in_pos[0] = 0;   in_vel[0] = 0;
    names[1] = "Mercury";    mass[1] = 3.3011e23;     orb_incl[1] = 7.00487;
    names[2] = "Venus";      mass[2] = 4.8675e24;     orb_incl[2] = 3.39471;
    names[3] = "Earth";      mass[3] = 5.9724e24;     orb_incl[3] = 0.00005;
    names[4] = "Mars";       mass[4] = 6.4171e23;     orb_incl[4] = 1.85061;
    names[5] = "Jupiter";    mass[5] = 1.89819e27;    orb_incl[5] = 1.30530;
    names[6] = "Saturn";     mass[6] = 5.6834e26;     orb_incl[6] = 2.48446;
    names[7] = "Uranus";     mass[7] = 8.6813e25;     orb_incl[7] = 0.76986;
    names[8] = "Neptune";    mass[8] = 1.02413e26;    orb_incl[8] = 1.76917;
    names[9] = "Pluto";      mass[9] = 1.303e22;      orb_incl[9] = 17.14175;

    //J2000, long_per in degree, axis in AU, ecc = eccentricity of the orbit
    long_per[0] =  0;          smajor_axis[0] =  0;            ecc[0] =  0;
    long_per[1] =  77.45645;   smajor_axis[1] =  0.38709893;   ecc[1] =  0.20563069;
    long_per[2] =  131.53298;  smajor_axis[2] =  0.72333199;   ecc[2] =  0.00677323;
    long_per[3] =  102.94719;  smajor_axis[3] =  1.00000011;   ecc[3] =  0.01671022;
    long_per[4] =  336.04084;  smajor_axis[4] =  1.52366231;   ecc[4] =  0.09341233;
    long_per[5] =  14.75385;   smajor_axis[5] =  5.20336301;   ecc[5] =  0.04839266;
    long_per[6] =  92.43194;   smajor_axis[6] =  9.53707032;   ecc[6] =  0.05415060;
    long_per[7] =  170.96424;  smajor_axis[7] =  19.19126393;  ecc[7] =  0.04716771;
    long_per[8] =  44.97135;   smajor_axis[8] =  30.06896348;  ecc[8] =  0.00858587;
    long_per[9] =  224.06676;  smajor_axis[9] =  39.48168677;  ecc[9] =  0.24880766;
    
    mean_long[0] = 0;            long_asc_node[0] = 0;
    mean_long[1] = 252.25084;    long_asc_node[1] = 48.33167;
    mean_long[2] = 181.97973;    long_asc_node[2] = 76.68069;
    mean_long[3] = 100.46435;    long_asc_node[3] = -11.26064;
    mean_long[4] = 355.45332;    long_asc_node[4] = 49.57854;
    mean_long[5] = 34.40438;     long_asc_node[5] = 100.55615;
    mean_long[6] = 49.94432;     long_asc_node[6] = 113.71504;
    mean_long[7] = 313.23218;    long_asc_node[7] = 74.22988;
    mean_long[8] = 304.88003;    long_asc_node[8] = 131.72169;
    mean_long[9] = 238.92881;    long_asc_node[9] = 110.30347;
    
    for(int i=1; i<NUM; i++)
        mean_anomaly[i] = mean_long[i] - long_per[i];

    cout<<setprecision(4)<<fixed<<right;    
    
    for(int i=0; i<NUM; i++) //now I convert masses in code-values
    {
        mass[i] /= m0;

        long_per[i] *= 2*M_PI/360; //deg -> rad
        orb_incl[i] *= 2*M_PI/360; 
        
        if(i!=0)
        {
            mean_long[i] *= 2*M_PI/360;
            mean_anomaly[i] *= 2*M_PI/360;
            long_asc_node[i] *= 2*M_PI/360;
        
            //find true anomaly
            g_M = mean_anomaly[i];
            g_e = ecc[i];
            bisection(eqEccentricityAnomaly, -2*M_PI,2*M_PI, 1.e-13, eccentricity_anomaly[i]);
            g_E = eccentricity_anomaly[i];
            
            true_anomaly[i] = 2*atan(sqrt((1+g_e)/(1-g_e))*tan(g_E/2));

            in_pos[i] = smajor_axis[i]*(1-ecc[i]*ecc[i])/(1+ecc[i]*cos(true_anomaly[i]));
            in_vel[i] = sqrt(2/in_pos[i] - 1/smajor_axis[i]);
        }
    }

    for(int i=0; i<NUM; i++)
    {
        if(i==0)
        {
            ipv[0] =  0;
            ipv[1] =  0;
            ipv[2] =  0;
            ipv[3] =  0;
            ipv[4] =  0;
            ipv[5] =  0;
        }
        else
        {
            int j;
            double v0;
            double nu,e,p,f; //true anomaly, eccentricity, longitude of perihelion, inclination of orbit
            double vp, vq; //velocity in perifocal system
            double rp, rq; 
            j = i*J;
            nu = true_anomaly[i];
            e = ecc[i];
            p = long_per[i];
            f = orb_incl[i];
            p = long_per[i]; 
            //p = long_asc_node[i] + M_PI/2;            
            //position in perifocal
            rp = in_pos[i]*cos(nu);
            rq = in_pos[i]*sin(nu);
            
            double rx,ry,rz; //rotation for inclination
            rx = rp*cos(f);
            ry = rq;
            rz = rp*sin(f);
           
            //rotation for perihelion
            ipv[0+j] =  cos(p)*rx - sin(p)*ry;  
            ipv[1+j] =  sin(p)*rx + cos(p)*ry;
            ipv[2+j] =  rz;
                
            //velocity
            v0 = in_vel[i]/sqrt( sin(nu)*sin(nu) + (e+cos(nu))*(e+cos(nu)) );
            vp = -v0*sin(nu);
            vq = v0*(e+cos(nu));
            
            double vx,vy,vz; //rotation for inclination
            vx = vp*cos(f);
            vy = vq;
            vz = vp*sin(f);
           
            //rotation for perihelion
            ipv[3+j] =  cos(p)*vx - sin(p)*vy;  
            ipv[4+j] =  sin(p)*vx + cos(p)*vy;
            ipv[5+j] =  vz;
        }
    }
    
    string fname = "SolarSys.dat";
    ofstream fdata;
    fdata.open(fname);
    fdata<<setiosflags(ios::scientific)<<fixed;
    
    for(int i=0; i<NEQ; i++)
        Y[i] = ipv[i]; //setting initial conditions  
    
    for(int i=1; i<NUM; i++)
        vx2[i] = Y[3+i*J];

    initial_clock = clock();
    
    while(1) //now we solve the ODEs using FR  
    {
        dt = dtheta*sqrt((Y[0+pY]*Y[0+pY]+Y[1+pY]*Y[1+pY]+Y[2+pY]*Y[2+pY])/(Y[3+pY]*Y[3+pY]+Y[4+pY]*Y[4+pY]+Y[5+pY]*Y[5+pY]));
        //dt = dtheta*r/v 

        for(int i=1; i<NUM; i++)
            vx1[i] = vx2[i];
        
        forestRuth3DStep(t, Y, dt, NEQ, RHS);
        
        for(int i=1; i<NUM; i++)
            vx2[i] = Y[3+i*J];
        
        t += dt; //we have to update t before printing, beacause Y is calculated at the t+dt time (using Y at t)   

        if(vx1[p]*vx2[p]<0) //if I have a turning point of the p planet 
        {
            turning_points[p]++;
            
            if(turning_points[p] == 1) //if the planet is at the first turning point 
            {
                t = 0; //we put t = 0 at the first turning point (we can do this because t doesn't appear explicity on ODEs)
                count_interval = interval-1; //because we want to print the first point 
                //now we update the ipv[]
                for(int i=0; i<NEQ; i++)
                    ipv[i] = Y[i];
            }
            
            if(turning_points[p] == 3) //at the first complete orbit after the previous "if" 
            {
                t_orb[p] = t;
                flag_t_orb[p]++;

                int min, sec = 0.7*p_orbits*(clock()-initial_clock)/CLOCKS_PER_SEC; //0.7 is an "empirical" constant 
                min = sec/60;
                sec = sec - 60*min;
                cout<<"Estimated computation time for "<<p_orbits<<" "<<names[p]<<"'s orbits ";
                cout<<"( dtheta = "<<dtheta<<" ): ";
                cout<<min<<" min  "<<sec<<" s"<<endl;
            }
            if(turning_points[p] == p_orbits*2-1) //we want to measure the period of the last orbit 
            {
                last_t = t; 
            }
            if(turning_points[p] == p_orbits*2 + 1) // +1 beacause we start to count to the first inv_point 
            {    
                fdata<<t; //we print the last line here because we break the while(1) cycle 
                for(int j=0; j<NEQ; j++)
                    fdata<<" "<<Y[j];
                fdata<<endl;
                
                last_t = t - last_t; //we save the last orbital period 
                break;
            }
        }
        
        //we can't measure time before the first inv_point because we put t = 0 when turning_points[p] = 1 
        if(turning_points[p]>0) 
        {
            for(int i=1; i<NUM; i++) //now we check if other planets have a turning point
            {
                if(i!=p) // the p planet has already be checked 
                {
                    if(vx1[i]*vx2[i]<0)
                    {
                        turning_points[i]++;

                        if(turning_points[i] == 1)
                            t_orb[i] = t;
                        
                        if(turning_points[i] == 3)
                        {
                            t_orb[i] = t - t_orb[i];
                            flag_t_orb[i]++;
                        }
                    }
                }
            }
        }

        if(turning_points[p] > 0) //we start to write on .dat if we've reach the first turning point of the planet p 
        {
            count_interval++;   
            if(count_interval == interval) //as already said, we print only some data on the .dat file 
            {
                fdata<<t;
                for(int j=0; j<NEQ; j++)
                    fdata<<" "<<Y[j];
                fdata<<endl;
                
                count_interval = 0; 
            }
        }
        
    }
    forestRuth3DStep(0,Y,dt,-1,RHS);//free the memory  
    
    cout<<endl;    
    
    double t_orb_exp[NUM]; //real orbital periods in earth's days;
    double t_orb_temp;
    t_orb_exp[0] = 0;
    t_orb_exp[1] = 87.969;
    t_orb_exp[2] = 224.65;
    t_orb_exp[3] = 365.256;
    t_orb_exp[4] = 687.0;
    t_orb_exp[5] = 4331;
    t_orb_exp[6] = 10747;
    t_orb_exp[7] = 30589;
    t_orb_exp[8] = 59800;
    t_orb_exp[9] = 90560;
    
    cout<<fixed;
    
    for(int i=1; i<NUM; i++)
    {
        cout<<setw(8)<<left<<names[i]<<":  ";

        t_orb_temp = t_orb[i]*t0/(24*3600);
      
        if(flag_t_orb[i]!=0)
        {
            cout<<setprecision(2)<<setw(8)<<right<<t_orb_temp<<" days  =  "<<setw(6)<<t_orb_temp/t_orb_exp[3]<<" ys";
            cout<<setprecision(4)<<setw(10)<<"err: "<<100*fabs(t_orb_temp - t_orb_exp[i])/t_orb_exp[i]<<" %"<<endl;
        }
        else
            cout<<setw(16)<<right<<"/"<<endl;
    }
    
    cout<<setprecision(2)<<endl<<"Experimental orbital periods: "<<endl;

    for(int i=1; i<NUM; i++)
    {
        cout<<setw(8)<<left<<names[i]<<":  ";
        cout<<setw(8)<<right<<t_orb_exp[i]<<" days  =  "<<setw(6)<<t_orb_exp[i]/t_orb_exp[3]<<" ys"<<endl;
    }

    #if !FIX_SUN
    cout<<"\n\nWarning: the sun is not fixed in (0,0), so the orbital periods are not precise."<<endl;
    #endif
    
    cout<<endl;
    cout<<setprecision(2)<<"Last orbital period of "<<names[p]<<":    "<<last_t*t0/(24*3600)<<" days"<<endl;
    cout<<setprecision(6)<<"Variation from the first orbit:  "<<100*fabs(t_orb[p]-last_t)/t_orb[p]<<" %"<<endl;

    int min, sec = (clock()-initial_clock)/CLOCKS_PER_SEC;
    min = sec/60; 
    sec = sec - 60*min;
    cout<<endl;    
    cout<<"computation time:  "<<min<<" min  "<<sec<<" s "<<endl;
    cout<<endl<<"File '"<<fname<<"' created."<<endl;
    fdata.close();
    

    return 0;
}

int bisection(double (*func)(double), double a, double b, double tol,double &zero)
{
    double xm, fa, fb, fxm;
    
    fa = func(a);
    fb = func(b);
    
    if(fa*fb > 0)
        return 0;
    
    while(1)
    {
        xm = (a+b)/2;
        fxm = func(xm);
        
        if( fabs(b-a) < tol)
        {
            zero = xm;
            return 1;
        }
        if(fa*fxm < 0) 
        {
            b = xm;
            fb = fxm;
        }
        else 
            if (fa*fxm >0)
            {
                a = xm;
                fa = fxm;
            }
            else //if it's exactly 0 (it can happens sometimes)
            {
                zero = xm;
                return 1;
            }
    }
}

void forestRuth3DStep(double t,double Y[], double h, int Neq, void (*myRHS)(double,double*,double*))
{
    double gamma = 1.351207191959658;
    static double *k, *v_half;  
    static int flag = 0;
    int N_particles, j=6, dim=3; //we don't use J and NUM so this function can be used in other codes 
    int l; //we use it in the for(for()) for j*i   

    N_particles = Neq/j;

    if(flag == 0) //first call 
    { 
        k = new double[Neq];
        v_half = new double[N_particles];
        flag++;
    }

    if(Neq == -1) //free memory
    {
        delete[] k;
        delete[] v_half;
        flag = 0;
	    return;
    }
    
    // 0  1  2   3   4   5
    // x  y  z   vx  vy  vz
    if(flag==1) //first call
    {
        myRHS(t, Y, k); //initialization of k 
        flag++; 
    }
    
    for(int i=0; i<N_particles; i++)
        for(int d=0; d<dim; d++)
        {
            l = j*i;
            Y[l+3+d] += gamma*h*0.5*k[l+3+d]; //d: 0->vx, 1->vy, 2->vz
            Y[l+d]   += gamma*h*Y[l+3+d];     //d: 0-> x, 1-> y, 2-> z
        }
    
    myRHS(t,Y,k);
    for(int i=0; i<N_particles; i++)
        for(int d=0; d<dim; d++)
        {
            l = j*i;
            Y[l+3+d] += (1-gamma)*h*0.5*k[l+3+d];
            Y[l+d]   += (1-2*gamma)*h*Y[l+3+d];

        }
    
    myRHS(t,Y,k);
    for(int i=0; i<N_particles; i++)
        for(int d=0; d<dim; d++)
        {
            l = j*i;
            Y[l+3+d] += (1-gamma)*h*0.5*k[l+3+d];
            Y[l+d]   += gamma*h*Y[l+3+d];
        }

    myRHS(t,Y,k);
    for(int i=0; i<N_particles; i++)
        for(int d=0; d<dim; d++)
        {
            l=j*i;
            Y[l+3+d] += gamma*h*0.5*k[l+3+d];
        }

    return;
}
void RHS(double t, double Y[], double k[])
{
    //0   1   2   3   4   5   
    //x   y   z   vx  vy  vz
    double r;
    double w; //weight of the interaction
    int p,f; //p index for particle, f index for field
    
    for(int i=0; i<NEQ;i++)
        k[i] = 0; 
    
    for(int i=0; i<NUM-1; i++)
        for(int j=i+1; j<NUM; j++)  
        {
            p = j*J;
            f = i*J;
            r = sqrt((Y[0+p]-Y[0+f])*(Y[0+p]-Y[0+f]) + (Y[1+p]-Y[1+f])*(Y[1+p]-Y[1+f]) + (Y[2+p]-Y[2+f])*(Y[2+p]-Y[2+f])); //distance ij 

            //i field on j particle 
            w = mass[i]/mass[0]; 
            k[0+p] = Y[3+p]; 
            k[1+p] = Y[4+p];
            k[2+p] = Y[5+p];
            k[3+p] += -w*(Y[0+p]-Y[0+f])/(r*r*r);
            k[4+p] += -w*(Y[1+p]-Y[1+f])/(r*r*r);
            k[5+p] += -w*(Y[2+p]-Y[2+f])/(r*r*r);

            //j field on i particle (p->f, f->p)  
            #if FIX_SUN 
            if(i!=0) //we want to fix the sun, so we have to not consider the planets' gravitational effects on it
            {        
                w = mass[j]/mass[0];
                k[0+f] = Y[3+f]; 
                k[1+f] = Y[4+f];    
                k[2+f] = Y[5+f];    
                k[3+f] += -w*(Y[0+f]-Y[0+p])/(r*r*r);
                k[4+f] += -w*(Y[1+f]-Y[1+p])/(r*r*r);
                k[5+f] += -w*(Y[2+f]-Y[2+p])/(r*r*r);
            }
            #else //otherwise we consider these effects 
            w = mass[j]/mass[0];
            k[0+f] = Y[3+f]; 
            k[1+f] = Y[4+f];    
            k[2+f] = Y[5+f];    
            k[3+f] += -w*(Y[0+f]-Y[0+p])/(r*r*r);
            k[4+f] += -w*(Y[1+f]-Y[1+p])/(r*r*r);
            k[5+f] += -w*(Y[2+f]-Y[2+p])/(r*r*r);
            #endif
        }
    
    return; 
}
double eqEccentricityAnomaly(double x)
{
    return x-g_e*sin(x)-g_M;    
}
