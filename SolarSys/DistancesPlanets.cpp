#include <iostream>
#include <fstream>
#include <cmath>
#include <iomanip>
#include <ctime>
#define J 6 
#define NUM_PLANETS 9
#define ROW_ELEM 61
#define NUM_ROW 20298

using namespace std;

int main()
{
    double line[ROW_ELEM]; 
    double r2_min[NUM_PLANETS][NUM_PLANETS], r2_new[NUM_PLANETS][NUM_PLANETS];
    double r2_max[NUM_PLANETS][NUM_PLANETS];
    clock_t my_clock;
    int p1, p2; //index for the planets 
    
    my_clock = clock();

    string fname = "SolarSys.dat";
    ifstream fdata; 
    fdata.open(fname);
    
    for(int m=0; m<NUM_PLANETS; m++)
        for(int n=0; n<NUM_PLANETS; n++)
        {
            r2_min[m][n] = 0;
            r2_max[m][n] = 0;
            r2_new[m][n] = 0; 
        }

    for(int i=0; i<NUM_ROW; i++)
    {
        for(int j=0; j<ROW_ELEM; j++) 
            if(j==0)
                fdata>>line[j];
            else
                fdata>>line[j-1]; //we don't need the first number of the line because is the time 
        
        if(i==0) //we have to initialize r2_min 
        {
            for(int m=0; m<NUM_PLANETS; m++) //r2 are symmetric matrix with zeros on diagonal
                for(int n=m+1; n<NUM_PLANETS; n++)
                {
                    p1 = J*(m+1);
                    p2 = J*(n+1);
                    
                    r2_min[m][n]  = (line[p1]-line[p2])*(line[p1]-line[p2]);
                    r2_min[m][n] += (line[p1+1]-line[p2+1])*(line[p1+1]-line[p2+1]);
                    r2_min[m][n] += (line[p1+2]-line[p2+2])*(line[p1+2]-line[p2+2]);
                    
                    r2_max[m][n]  = r2_min[m][n];
                }
        }
        else
        {
            for(int m=0; m<NUM_PLANETS; m++)
                for(int n=m+1; n<NUM_PLANETS; n++)
                {
                    p1 = J*(m+1);
                    p2 = J*(n+1);
                    
                    r2_new[m][n]  = (line[p1]-line[p2])*(line[p1]-line[p2]);
                    r2_new[m][n] += (line[p1+1]-line[p2+1])*(line[p1+1]-line[p2+1]);
                    r2_new[m][n] += (line[p1+2]-line[p2+2])*(line[p1+2]-line[p2+2]);
                    
                    if(r2_new[m][n] < r2_min[m][n])
                        r2_min[m][n] = r2_new[m][n];
                    
                    if(r2_new[m][n] > r2_max[m][n])
                        r2_max[m][n] = r2_new[m][n];
                }
        }
    } 

    for(int m=1; m<NUM_PLANETS; m++)
        for(int n=0; n<m; n++)
        {
            r2_min[m][n] = r2_min[n][m];
            r2_max[m][n] = r2_max[n][m];
        }
    cout<<"Min distances between planets in AU: "<<endl;
    cout<<setprecision(4)<<fixed<<right;

    cout<<setw(9)<<" ";
    for(int n=0; n<NUM_PLANETS; n++)
        cout<<setw(9)<<n+1<<" ";
    cout<<endl;

    for(int m=0; m<NUM_PLANETS; m++)
    {
        cout<<setw(9)<<m+1<<" ";
        
        for(int n=0; n<NUM_PLANETS; n++)
            cout<<setw(9)<<sqrt(r2_min[m][n])<<" ";

        cout<<endl;
    }
    cout<<endl;    
    cout<<"Max distances between planets in AU: "<<endl;
    cout<<setprecision(4)<<fixed<<right;

    cout<<setw(9)<<" ";
    for(int n=0; n<NUM_PLANETS; n++)
        cout<<setw(9)<<n+1<<" ";
    cout<<endl;

    for(int m=0; m<NUM_PLANETS; m++)
    {
        cout<<setw(9)<<m+1<<" ";
        
        for(int n=0; n<NUM_PLANETS; n++)
            cout<<setw(9)<<sqrt(r2_max[m][n])<<" ";

        cout<<endl;
    }

    cout<<endl<<"Calculation time: "<<(clock()-my_clock)/CLOCKS_PER_SEC<<endl;
    fdata.close();

    return 0;
}
