# README

In this repository you will find some codes that I have written during the college.
You can find more informations for every code in NameOfCode/infoNameOfCode.txt 


Name                  short description                          language             year
-------------------------------------------------------------------------------------------
CompressPNG           compress png using k-means                  Python              2020
                      (three different implementations)
-------------------------------------------------------------------------------------------
NeuralNetworkCpp      Neural network from scracth (without         C++                2020
                      using non standard libraries) for 
                      handwritten digit recognition
                      with one hidden layer. 
                      This was my first NN (and last in C++),
                      so it is "naive"
-------------------------------------------------------------------------------------------
SolarSys              Numerical simulation of the Solar            C++                2019
                      System using a symplectic algorithm
-------------------------------------------------------------------------------------------
GameOfLife            Game of Life by the mathematician             C                 2016
                      John Conway
-------------------------------------------------------------------------------------------

* Simone Albanesi
