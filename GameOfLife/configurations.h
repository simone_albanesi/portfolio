//prototypes;
void imput_control(int,int,int*,int*);
void generate_glider_NO(char **mtr, int M, int N);
void generate_glider_NE(char **mtr, int M, int N);
void generate_glider_SO(char **mtr, int M, int N);
void generate_glider_SE(char **mtr, int M, int N);
void generate_toad(char **mtr, int M, int N);
void generate_blinker_horizontal(char **mtr, int M, int N);
void generate_blinker_vertical(char **mtr, int M, int N);
void generate_block(char **mtr, int M, int N);
void generate_diehard(char **mtr, int M, int N);
void generate_acorn(char **mtr, int M, int N);

//definitions;
void imput_control(int M, int N, int* coord1, int* coord2) //control that the configuration is entirely in the grid
{
    do 
    {
        if(*coord1 < 2 || *coord1 > (M-1) || *coord2 > (N-1) || *coord2 <  2)
        {
            printf("Invalid imput, too closer to the grid's edge!\n");
            printf("New imput:\n");
            scanf("%d%d", coord1, coord2);
        }

    } while(*coord1 < 2 || *coord1 > (M-1) || *coord2 > (N-1) || *coord2 <  2);
    
    return;
}
// configurations
void generate_glider_NO(char **mtr, int M, int N)
{
    int coord1, coord2;

    printf("Insert the coordinates of the glinder (glider's centre):\n");
    scanf("%d%d", &coord1, &coord2);
    
    imput_control(M,N,&coord1,&coord2);

    mtr[coord1][coord2] = '.'; // glinder's centre
    mtr[coord1+1][coord2] = 'X'; //head
    mtr[coord1][coord2-1] = 'X';
    mtr[coord1-1][coord2-1] = 'X';
    mtr[coord1-1][coord2] = 'X';
    mtr[coord1-1][coord2+1] = 'X';
}

void generate_glider_NE(char **mtr, int M, int N)
{
    int coord1, coord2;

    printf("Insert the coordinates of the glinder (glider's centre):\n");
    scanf("%d%d", &coord1, &coord2);
    
    imput_control(M,N,&coord1,&coord2);

    mtr[coord1][coord2] = '.'; //glinder's center
    mtr[coord1][coord2-1] = 'X'; //head
    mtr[coord1-1][coord2] = 'X';
    mtr[coord1-1][coord2+1] = 'X';
    mtr[coord1][coord2+1] = 'X';
    mtr[coord1+1][coord2+1] = 'X';
}

void generate_glider_SO(char **mtr, int M, int N)
{
    int coord1, coord2;

    printf("Insert the coordinates of the glinder (glider's centre):\n");
    scanf("%d%d", &coord1, &coord2);
    
    imput_control(M,N,&coord1,&coord2);

    mtr[coord1][coord2] = '.'; //glinder's head
    mtr[coord1][coord2+1] = 'X'; //head
    mtr[coord1+1][coord2] = 'X';
    mtr[coord1-1][coord2-1] = 'X';
    mtr[coord1][coord2-1] = 'X';
    mtr[coord1+1][coord2-1] = 'X';
}

void generate_glider_SE(char **mtr, int M, int N)
{
    int coord1, coord2;

    printf("Insert the coordinates of the glinder (glider's centre):\n");
    scanf("%d%d", &coord1, &coord2);
    
    imput_control(M,N,&coord1,&coord2);

    mtr[coord1][coord2] = '.'; //glinder's centre
    mtr[coord1-1][coord2] = 'X';//head
    mtr[coord1][coord2+1] = 'X';
    mtr[coord1+1][coord2-1] = 'X';
    mtr[coord1+1][coord2] = 'X';
    mtr[coord1+1][coord2+1] = 'X';

}

void generate_toad(char **mtr, int M, int N)
{
    int coord1, coord2;

    printf("Insert the coordinates of the toad (toad's centre):\n");
    scanf("%d%d", &coord1, &coord2);

    imput_control(M,N,&coord1,&coord2);

    mtr[coord1][coord2-1] = 'X';
    mtr[coord1][coord2] = 'X';
    mtr[coord1][coord2+1] = 'X';
    mtr[coord1-1][coord2] = 'X';
    mtr[coord1-1][coord2+1] = 'X';
    mtr[coord1-1][coord2+2] = 'X';
}

void generate_blinker_horizontal(char **mtr, int M, int N)
{
    int coord1, coord2;
 
    printf("Insert the coordinates of the blinker (blinker's centre):\n");
    scanf("%d%d", &coord1, &coord2);
    
    imput_control(M,N,&coord1,&coord2);

    mtr[coord1][coord2-1] = 'X';
    mtr[coord1][coord2] = 'X';
    mtr[coord1][coord2+1] = 'X';
}

void generate_blinker_vertical(char **mtr, int M, int N)
{
    int coord1, coord2;

    printf("Insert the coordinates of the blinker (blinker's centre):\n");
    scanf("%d%d", &coord1, &coord2);
    
    imput_control(M,N,&coord1,&coord2);

    mtr[coord1-1][coord2] = 'X';
    mtr[coord1][coord2] = 'X';
    mtr[coord1+1][coord2] = 'X';
}

void generate_block(char **mtr, int M, int N)
{
    int coord1, coord2;

    printf("Insert the coordinates of the block (top-left corner):\n");
    scanf("%d%d", &coord1, &coord2);
    
    imput_control(M,N,&coord1,&coord2);

    mtr[coord1][coord2] = 'X';
    mtr[coord1][coord2+1] = 'X';
    mtr[coord1+1][coord2] = 'X';
    mtr[coord1+1][coord2+1] = 'X';
}

void generate_diehard(char **mtr, int M, int N)
{
    int coord1, coord2;
	
    printf("Insert the coordinates of the die hard (central cell):\n");
    scanf("%d%d", &coord1, &coord2);
    
    imput_control(M,N,&coord1,&coord2);

    mtr[coord1-1][coord2+3] = 'X';
    mtr[coord1][coord2-2] = 'X';
    mtr[coord1][coord2-3] = 'X';
    mtr[coord1+1][coord2-2] = 'X';
    mtr[coord1+1][coord2+2] = 'X';
    mtr[coord1+1][coord2+3] = 'X';
    mtr[coord1+1][coord2+4] = 'X';
}

void generate_acorn(char **mtr, int M, int N)
{
    int coord1, coord2;
	
    printf("Insert the coordinates of the acorn (acorn's centre):\n");
    scanf("%d%d", &coord1, &coord2);
    
    imput_control(M,N,&coord1,&coord2);

    mtr[coord1][coord2] = 'X';
    mtr[coord1-1][coord2-2] = 'X';
    mtr[coord1+1][coord2-3] = 'X';
    mtr[coord1+1][coord2-2] = 'X';
    mtr[coord1+1][coord2+1] = 'X';
    mtr[coord1+1][coord2+2] = 'X';
    mtr[coord1+1][coord2+3] = 'X';
}
