/* 
Game of Life by John Conway 
'.' -> dead cell
'x' -> live cell

Rules:
- Underpopulation: Any live cell with fewer than two live neighbours dies
- Overpopultation: Any live cell with more than three live neighbours dies
- Reproduction:    Any dead cell with exactly three live neighbours becomes a live cell
- Survival:        Any live cell with two or three live neighbours lives on to the next generation.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "configurations.h"
#define MAX_ROW 100
#define MAX_COL 250
#define MIN 10

//global variables
static int M, N; 
static int count_cells;
static int generation;

//Prototypes;
void print_matrix(char **mtr);
int  count_cells_alive(char **mtr, int i, int j);
void wait(float seconds);
void apply_rules(char **mtr, char **mtr_copy);
void skip_to_gen(char  **mtr, char **mtr_copy, int generation);

int main()
{
    int i,j,k; 
    int x,y; //coordinates;
    char choice;
    int flag;
    int number;
    char **matrix;
    char **matrix_copy;
    char **matrix_zero;
    int taken_cells;
    float mytime;
    int configuration;

    //+++initialization+++;

    printf("%s","Welcom in the Game of Life by John Conway.\n\nEnter the number of rows and columns of the grid:\n");
    printf("Max row: %d\nMax columns: %d\n", MAX_ROW, MAX_COL);
    printf("(suggested: 30 120\tmin: %d %d)\nInput: ", MIN, MIN);
    scanf("%d%d", &M, &N);
    
    do //input control
    {
        if( M>MAX_ROW || N>MAX_COL || N < MIN || M < MIN )
        {
            printf("Invalid input!\nEnter the number of rows and columns of the grid:\n");
            scanf("%d%d", &M, &N);
        }
    } while ( M>MAX_ROW || N>MAX_COL || N < MIN || M < MIN );

    if(N > 50)
        printf("%s", "\nColumns indices will be printed in vertical\n\n");

    //allocate matrix;
    matrix = (char**) malloc((M+2)*(N+2)*sizeof(char*));
    // We have to allocate (M+2)*(N+2) because we must have a margin (margin will not be displayed on screen) 
    if(matrix == NULL)
    {
        puts("\nError in the memory allocation.");
        return 0;
    }

    for(i=0; i<(M+2); i++)
    {
        matrix[i] = (char*) malloc((N+2)*sizeof(char));

        if(matrix[i] == NULL)
        {
            puts("\nError in the memory allocation.");
            return 0;
        }

        memset(matrix[i], '.', N+2); //Initialize the cells of the i-row with '.' (dead cell);
    }

    //copy;
    matrix_copy = (char**) malloc((M+2)*(N+2)*sizeof(char*));

    if(matrix == NULL)
    {
        puts("\nError in the memory allocation.");
        return 0;
    }

    for(i=0; i<(M+2); i++)
    {
        matrix_copy[i] = (char*) malloc((N+2)*sizeof(char));

        if(matrix[i] == NULL)
        {
            puts("\nError in the memory allocation.");
            return 0;
        }
    }

    //print grid without margin
    print_matrix(matrix);

    printf("%s", "\n \nDigit 0     - insert a number of live cells randomly\n");
    printf("%s",      "Another key - insert cells 'by hand' or using pre-built configuration\nInput: ");
    scanf(" %c", &choice);

    if(choice == '0') //random;
    {
        printf("\nEnter the number of live cells to generate randomly (up to 75%% of the grid): ");
        scanf("%d", &number);

        do //input control;
        {
            if( number<0 || number > ((M+2)*(N+2))*3/4 )
            {
                printf("Invalid input. Enter again the number of live cell to generate: ");
                scanf("%d", &number);
            }
        } while ( number<0 || number > ((M+2)*(N+2))*3/4 );


        srand(time(NULL)); //seed

        flag = 0;

        while( flag < number )
        {
            x = rand()%M + 1; 
            y = rand()%N + 1; 

            if(matrix[x][y] == '.') //if the cell is dead
            {
                matrix[x][y] = 'X'; //it become alive
                flag++;
            }
        }
    }
    else //insertion by hand or with pre-built configurations
    {
        taken_cells = 0; 

        printf("\nDigit 0 if you want to insert pre-built configurations, otherwise digit another key\nInput: ");
        scanf(" %c", &choice);
        
        if(choice == '0')
        {
            do
            {
                printf("\nLists of pre-built configurations.\n");
                printf("1%3c glider(go north-west);\n2%3c glider(go north-east);\n", '-', '-');
                printf("3%3c glider(go south-west);\n4%3c glider(go south-east);\n", '-', '-');
                printf("5%3c toad;\n6%3c horizontal blinker;\n7%3c vertical blinker;\n8%3c block;\n",'-', '-','-', '-');
                printf("9%3c die hard (if it is left alone, dies in 130 generations);\n10%2c acorn;\n",'-', '-');
                printf("0%3c end\n", '-');
                printf("Enter choice: ");
                scanf("%d", &configuration);

                // be aware that in you overlap pre-built configuration, taken_cells will be greater than the effective
                // number of taken cells
                switch (configuration)
                {
                case 1:
                    generate_glider_NO(matrix, M, N);
                    taken_cells += 5; //number of cells that this configuration takes
                    print_matrix(matrix);
                    break;

                case 2:
                    generate_glider_NE(matrix, M, N);
                    taken_cells += 5;
                    print_matrix(matrix);
                    break;

                case 3:
                    generate_glider_SO(matrix, M, N);
                    taken_cells += 5;
                    print_matrix(matrix);
                    break;

                case 4:
                    generate_glider_SE(matrix, M, N);
                    taken_cells += 5;
                    print_matrix(matrix);
                    break;

                case 5:
                    generate_toad(matrix, M, N);
                    taken_cells += 6; 
                    print_matrix(matrix);
                    break;

                case 6:
                    generate_blinker_horizontal(matrix, M, N);
                    taken_cells += 3; 
                    print_matrix(matrix);
                    break;

                case 7:
                    generate_blinker_vertical(matrix, M, N);
                    taken_cells += 3; 
                    print_matrix(matrix);
                    break;

                case 8:
                    generate_block(matrix, M, N);
                    taken_cells += 4;
                    print_matrix(matrix);
                    break;

                case 9:
                    generate_diehard(matrix, M, N);
                    taken_cells += 7;
                    print_matrix(matrix);
                    break;

                 case 10:
                    generate_acorn(matrix, M, N);
                    taken_cells += 7;
                    print_matrix(matrix);
                    break;

                case 0:
                    break;

                default:
                    printf("Invalid input.\n");
                }
            } while (configuration != 0);
        }

        printf("\nEnter the number of live cells to insert by hand (up to 75%% of free cells): ");
        scanf("%d", &number);

        do // input control
        {
            if( number < 0 || number > ((M+2)*(N+2) - taken_cells)*3/4 )
            {
                printf("Invali input. Enter again the number: ");
                scanf("%d", &number);
            }
        } while ( number<0 || number > ((M+2)*(N+2) - taken_cells)*3/4 );

        flag = 0;

        while( flag < number )
        {
            printf("Enter the coordinates of the cell n.%d to make alive:\n", flag + 1); 
            scanf("%d%d", &x, &y);
            
            do //input control
            {
                if( x>M || y>N || x<1 || y<1 )
                {
                    printf("Invalid input. Enter again the coordinates: \n");
                    scanf("%d%d", &x, &y);
                }
            } while ( x>M || y>N || x<1 || y<1 );


            if(matrix[x][y] == 'X')
            {
                printf("This cell is already alive!!\n");
            }
            else
            {
                matrix[x][y] = 'X';

                //print grid
                print_matrix(matrix);

                flag++;
            }
        }
    }
    //+++end of initialization+++

    //copy matrix in matrix_copy;
    for(i=0; i<(M+2); i++)
        for(j=0; j<(N+2); j++)
            matrix_copy[i][j] = matrix[i][j];

    generation = 0;
    printf("\n\nGeneration n.%d\n", generation++); 
    print_matrix(matrix);

    printf("\n\nEnter your choice:\n");
    printf("0 - go to next generation when you press Enter\n");
    printf("1 - go to next generation after a choosen time interval (not the best choice for Windows user)\n");
    printf("2 - enter the generation to display\n");

    printf("\nBe aware that if you are a Windows user, the second choice it's not the best one, ");
    printf("because the Command Prompt is 'slow' to print the grid.\nInstead if you are a Linux user ");
    printf("there are no problems.\n\nInput: ");
    scanf("%d", &flag);
    
    do //input control
    {
        if(flag != 0 && flag != 1 && flag != 2)
        {
            printf("Invalid input.\nInput: ");
            scanf("%d", &flag);
        }
    } while (flag != 0 && flag != 1 && flag != 2);


    //start evolution with the choosen method
    if(flag == 0)
    {
        int max_gen;
        printf("%s", "\nInsert max generation (program will stop after this generation): ");
        scanf("%d", &max_gen);

        while(generation<=max_gen)
        {
            printf("\n\nGeneration n.%d\n", generation);
            apply_rules(matrix, matrix_copy);
            print_matrix(matrix);
            generation++;
            printf("%s","\nPress Enter to continue...");
            getchar();
        }
    }
    if(flag == 1)
    {
        printf("%s", "\nEnter the time interval (it is a float): ");
        scanf("%f", &mytime);
        int max_gen;
        printf("%s", "\nInsert max generation (programm will stop after this generation): ");
        scanf("%d", &max_gen);

        while(generation<=max_gen) 
        {
            printf("\n\n\nGeneration n.%d.\n", generation); 
            apply_rules(matrix,matrix_copy);
            print_matrix(matrix);
            generation++;
            wait(mytime);
        }
    }
    if (flag == 2)
    {
        //we need the initial configuration;
        matrix_zero = (char**) malloc (M*N*sizeof(char*));

        if(matrix == NULL)
        {
            puts("\nError in the memory allocation.");
            return 0;
        }

        for(i=0; i<M; i++)
        {
            matrix_zero[i] = (char*) malloc(N*sizeof(char));

            if(matrix[i] == NULL)
            {
                puts("\nError in the memory allocation.");
                return 0;
            }
        }

        //copy in matrix_zero;
        for(i=0; i<M; i++)
            for(j=0; j<N; j++)
                matrix_zero[i][j] = matrix[i][j];

        do
        {
            printf("Enter the generation to display (enter -1 to end): ");
            scanf("%d", &generation);
            if( generation >= 0)
            {
                skip_to_gen(matrix, matrix_copy, generation);

                printf("\n\n\nGeneration n.%d.\n", generation);
                print_matrix(matrix);

                //go to first gen (0 generation);
                for(i=0; i<M; i++)
                    for(j=0; j<N; j++)
                        matrix[i][j] = matrix_zero[i][j];

                for(i=0; i<M; i++)
                    for(j=0; j<N; j++)
                        matrix_copy[i][j] = matrix_zero[i][j];
             }
        } while ( generation >= 0 );
        
        free(matrix_zero);
    }

    free(matrix);
    free(matrix_copy);

 return 0;
}


//Definition;
void print_matrix(char **mtr)
{
    int e,f;
    int count, tens, hundreds;

    if( N <= 50 )
    {
        printf("%3c", ' ');

        for(e=1; e<(N+1); e++)
            printf("%3d", e);

        for(e=1; e<(M+1); e++)
        {
            printf("\n%3d", e);

            for(f=1; f<(N+1); f++)
                printf("%3c", mtr[e][f]);
        }

        printf("%s", "\n");
    }
    else
    {
        printf("%3c", ' ');

        count = 0;
        tens = 0;
        hundreds = 0;

        for(e=1; e < (N+1); e++) //print hundreds
        {
            if( hundreds != 0)
                printf("%d", hundreds);
            else
                printf("%s"," ");

            count++;

            if(count == 99)
            {
                count = -1;
                hundreds++;
            }
        }

        printf("\n%3c", ' ');

        count = 0;
        tens = 0;

        for(e=1; e<(N+1); e++) //print tens;
        {
            if(e < 10)
                printf("%s", " ");

            if( e >= 10)
                printf("%d", tens);

            count++;

            if(count == 9)
            {
                count = -1;
                tens++;
            }

            if(tens == 10)
                tens = 0;
        }

        printf("\n%3c", ' ');

        tens = 0;
        count = 0;

        for(e=1; e<(N+1); e++) //print units;
        {
            printf("%d", e - tens*10);
            count++;

            if(count == 9)
            {
                count = -1;
                tens++;
            }
        }


        for(e=1; e<(M+1); e++)
        {
            printf("\n%3d", e);

            for(f=1; f<(N+1); f++)
                printf("%c", mtr[e][f]);
        }

        printf("%s", "\n");
    }
}

int count_cells_alive(char **mtr, int i, int j)
{
    int k;
    int count;

    count=0;
    
    for(k = -1; k <= 1; k++) //check the row above
        if(mtr[i-1][j+k] == 'X')
                count++;

    for(k = -1; k <= 1; k++) //check the row below
        if(mtr[i+1][j+k] == 'X')
            count++;

    if(mtr[i][j-1] == 'X')
        count++;

    if(mtr[i][j+1] == 'X')
        count++;

    return count;
}

void wait(float seconds)
{
    clock_t fine;
    fine = clock() +(int)(seconds*CLOCKS_PER_SEC); //cast
    while(clock() < fine){}
}
void apply_rules(char **mtr, char **mtr_copy)
{
    int i,j;
    int count_cells;

    for(i=1; i<=M; i++)
        for(j=1; j<=N; j++)
        {
            count_cells = 0;

            if(mtr[i][j] == '.')
            {
                count_cells = count_cells_alive(mtr, i, j);

                if(count_cells == 3)
                    mtr_copy[i][j] = 'X';
            }
            else 
            {
                count_cells = count_cells_alive(mtr, i, j);

                if(count_cells < 2)
                    mtr_copy[i][j] = '.';

                if(count_cells > 3)
                    mtr_copy[i][j] = '.';
            }
        }

        for(i=0; i<(M+2); i++)
            for(j=0; j<(N+2); j++)
                mtr[i][j] = mtr_copy[i][j];

    return;
}
void skip_to_gen(char  **mtr, char **mtr_copy, int generation)
{
    int i, j;
    int k;

    for(k=0; k<generation; k++)
            apply_rules(mtr,mtr_copy);

    return;
}
