#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr  9 16:58:55 2020

@author: simone
"""

# Image compression with K-means algorithm 
# Consider an image with MxN pixels, each pixel is rappresented with three 
# 8-bit unsigned integer (from 0 to 255). The programm maps all these color in 
# K colors, so every pixel is rappresented by a 4-bit integer

import argparse
import numpy as np
from skimage import io
from time import perf_counter
import warnings 

parser = argparse.ArgumentParser(prog='compressPNG_naive', description='compress PNG files using k-means algorithm')
parser.add_argument('-i', '--input', type=str, dest='fname', help='name of the input picture')
parser.add_argument('-k', type=int, dest='K', help='number of colors (i.e. clusters)', default=16)
parser.add_argument('-n', '--niters', type=int, dest='iters', help='number of iterations', default=10)
parser.add_argument('-c', '--cycles', type=int, dest='num_init', help='number of initialization cycles', default=1)

args     = parser.parse_args()
fname    = args.fname;
K        = args.K;
iters    = args.iters;
num_init = args.num_init;

if fname==None:
    print('Error! You need to insert the name of the input picture')

if K > 255:
    K = 255

t0 = perf_counter()
img = io.imread(fname)
img = img/255 # 1:255 -> 0:1 

n = len(img[0,0])
m = len(img)*len(img[0])
X = np.reshape(img, (m, n), order='C')

#initialize to zero so we see clearly the dimensions
centroids = np.zeros((K,n))
clabels   = np.zeros(m) #every examples is linked to a centroid

# +++++++++++++++++ function to use in the k-means algorithm ++++++++++++++

# initialize centroids 
def initCentroids(X):
    X_copy = X.copy()
    np.random.shuffle(X_copy)
    return X_copy[:K]

def closestCentroids(X, centroids):
    dist = np.array([np.linalg.norm(X - c, axis=1) for c in centroids])
    return np.argmin(dist, axis=0)  
            
def newCentroids(X, clabels, centroids):
    K = len(centroids)
    with warnings.catch_warnings():
        warnings.simplefilter("ignore", category=RuntimeWarning)
        out = np.array([X[clabels==k].mean(axis=0) for k in range(K)])
    return out

def cost(X,centroids,clabels):
    mysum = 0;
    for k in range(len(centroids)):
        diff = np.array(X[clabels==k]-centroids[k])
        dist = np.array([np.linalg.norm(diff, axis=1)])
        mysum += np.sum(dist)
    return mysum/m

# +++++++++++++++++++++++++++++  k-means algorithm +++++++++++++++++++++++++
t0_k = perf_counter()
centroids_final = np.zeros((K,n))
clabels_final = np.zeros((m))

for i in range(num_init):
    centroids = initCentroids(X)
    
    for j in range(0, iters):
        clabels = closestCentroids(X, centroids)
        centroids = newCentroids(X,clabels,centroids)

        
    if i == 0: # verify if these are the best centroids 
        J_min = cost(X,centroids,clabels)
        centroids_final = centroids.copy()
        clabels_final = clabels.copy()
        idx = 0;
    else:
        J = cost(X, centroids,clabels)
        if J<J_min:
            J_min = J
            centroids_final = centroids.copy()
            clabels_final = clabels.copy()
            idx = i
            
    print("cycle: ", i+1, "/", num_init)
        
centroids = centroids_final.copy() # take best centroids
clabels = clabels_final.copy()
        
print("\nTime of k-means: ", perf_counter()-t0_k)   

# ++++++++++++++++++++++++++++ Recover the image ++++++++++++++++++++++++++
X_compressed = np.zeros((m,n))

for i in range(0,m):
    X_compressed[i] = centroids[int(clabels[i])]

img_compressed = 255*np.reshape(X_compressed,
                                (len(img),len(img[0]), n), order='C')
img_compressed = img_compressed.astype(np.uint8)

io.imshow(img_compressed)
new_fname = fname[:-4]+"_opt_compress"+str(K)+".png"
io.imsave(new_fname, img_compressed)

print('\n', new_fname, ' saved', sep='')
print("\nTotal time (after input): ", perf_counter()-t0)
