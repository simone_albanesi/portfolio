#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr  9 02:09:23 2020

@author: simone
"""


# Image compression with K-means algorithm 
# Consider an image with MxN pixels, each pixel is rappresented with three 
# 8-bit unsigned integer (from 0 to 255). The programm maps all these color in 
# K colors, so every pixel is rappresented by a 4-bit integer

import argparse
import numpy as np
from skimage import io
from time import perf_counter

parser = argparse.ArgumentParser(prog='compressPNG_naive', description='compress PNG files using k-means algorithm')
parser.add_argument('-i', '--input', type=str, dest='fname', help='name of the input picture')
parser.add_argument('-k', type=int, dest='K', help='number of colors (i.e. clusters)', default=16)
parser.add_argument('-n', '--niters', type=int, dest='iters', help='number of iterations', default=10)
args = parser.parse_args()

fname = args.fname;
K     = args.K;
iters = args.iters;

if fname==None:
    print('Error! You need to insert the name of the input picture')

if K > 255:
    K = 255
    
t0 = perf_counter()
img = io.imread(fname)
img = img/255 # 1:255 -> 0:1 

n = len(img[0,0])
m = len(img)*len(img[0])
X = np.reshape(img, (m, n), order='C')

#initialize to zero so we see clearly the dimensions
centroids = np.zeros((K,n))
clabels   = np.zeros(m) #every examples is linked to a centroid

# initialize centroids 
X_copy = X.copy()
np.random.shuffle(X_copy)
centroids = X_copy[:K]

# +++++++++++++++++ function to use in the k-means algorithm ++++++++++++++
def closestCentroids(X, centroids):
    m = len(X)
    K = len(centroids)
    for i in range(0, m):
        min_dist = np.linalg.norm(X[i]-centroids[0])
        clabels[i] = 0
        for j in range(1,K):
            dist = np.linalg.norm(X[i]-centroids[j])
            if dist<min_dist:
                min_dist = dist
                clabels[i] = j
    return clabels        

            
def newCentroids(X, clabels, K):
    n = len(centroids[0])
    new_centroids = np.zeros((K,n))
    for i in range(0,K):
        norm = 0
        for j in range(0,len(X)):
            if clabels[j] == i:
                norm += 1
                new_centroids[i] = np.add(new_centroids[i], X[j])
        new_centroids[i] /= norm   
    return new_centroids

# +++++++++++++++++++++++++++++  k-means algorithm +++++++++++++++++++++++++
t0_k = perf_counter()
for i in range(0, iters):
    clabels = closestCentroids(X, centroids)
    centroids = newCentroids(X,clabels,K)
    print("iter: ", i+1, "/", iters)   
print("\nTime of k-means: ", perf_counter()-t0_k)   

# ++++++++++++++++++++++++++++ Recover the image ++++++++++++++++++++++++++
X_compressed = np.zeros((m,n))

for i in range(0,m):
    X_compressed[i] = centroids[int(clabels[i])]

img_compressed = 255*np.reshape(X_compressed,
                                (len(img),len(img[0]), n), order='C')

img_compressed = img_compressed.astype(np.uint8)

io.imshow(img_compressed)
new_fname = fname[:-4]+"_naive_compress"+str(K)+".png"
io.imsave(new_fname, img_compressed)

print('\n', new_fname, ' saved', sep='')
print("\nTotal time (after input): ", perf_counter()-t0)
