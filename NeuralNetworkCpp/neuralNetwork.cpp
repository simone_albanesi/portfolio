/* 
    Neural Network for multi-class classification problem with 1 hidden layer
    In the first part of this code we will train the network for digit-recognition on the data that you can find in data/ 
    
    Read 'infoNN.txt' for more info on this programm and data/note.txt for more details on the data
*/

#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <iomanip>
#include <ctime>
#include "header.h"
#define VERBOSE 1
#define NEURH 25   // number of neuron in the hidden layer
#define N 400       // number of features (i.e. 400 for 20x20 pixels images)
#define KK 10       // number of labels

using namespace std;

static double g_alpha = 0.01; // learning rate
static double g_lambda = 1;    // reg parameter

double batchCostFunction(int,Matrix &,Matrix &,Matrix &,Matrix &,Matrix &,Matrix &); 
void GradDescent(int,Matrix,Matrix,Matrix &,Matrix &, Matrix &, Matrix &, int iterations,bool isBatch);
void predictAllX(int,Matrix,Matrix,Matrix,Matrix &);
int predict(int,Matrix,Matrix,Matrix);

int main()
{
    int m=5000; //number of training sets
    int iter; // iteration of GradDescent
    bool useOrTrain; // if true, read a 20x20 pgm and tell what digit is, else Train the NN 
    bool thetaInput; // if true, use the Thata1,2.dat files, else randomly initialize the weights
    bool saveOutput; // if true, save the weithgs found  
    string data_name = "X.dat";     // name of the file where there are the data
    string labels_name = "y.dat";   // name of the file with the labels
    Matrix X(m,N), y(m,1); //matrix that will contain the data
    Matrix Theta1(NEURH, N+1), Theta2(KK, NEURH+1);   // weights and gradient
    Matrix dTheta1(NEURH, N+1), dTheta2(KK, NEURH+1); 
    Matrix y_logic(m,KK); 

    //++++++++++++++++++++++++++++++++++++++++++ Input and read Data ++++++++++++++++++++++++++++++++++
    iter = 10;        // you have to make your choices here if you set VERBOSE 0
    thetaInput = true;
    saveOutput = true;

    #if VERBOSE 
    cout<<endl; 
    cout<<"Neural Network with 1 hidden layer with "<<NEURH<<" neurons"<<endl;
    cout<<"Task: recognition of handwritten digits"<<endl;
    cout<<"Input: 20x20 grayscale images in .pgm format (P2)"<<endl<<endl;
    
    cout<<"0 - give .pgm images as imput and get the digits"<<endl;
    cout<<"1 - train the neural network"<<endl<<"Input: ";
    cin>>useOrTrain;
    cout<<"You have choosen: "<<useOrTrain<<endl<<endl;
    
    
    if(useOrTrain == false)
    {
        string fname1, fname2;
        fname1 = "Theta1.dat";
        fname2 = "Theta2.dat";
    
        bool flag = true;

        if(!readDatMatrix(fname1, Theta1))
            flag = false;

        if(!readDatMatrix(fname2, Theta2))
            flag = false;   
        
        if( flag == false ) 
        {
            cout<<"Error. In order to read handwritten digit, you need: "<<endl;
            cout<<"- "<<fname1<<endl;
            cout<<"- "<<fname2<<endl;
            cout<<"Train the network if you don't have them"<<endl;
            return -1;
        }
        
        int dim = 20;
        Matrix Row(1, dim*dim);
        string input_name;
        string exit = "exit";
        
        do
        {
            cout<<"Enter the name of the pgm file to read ('exit' to terminate): "<<endl;
            cin>>input_name;

            if(PGMToRow(input_name, dim, Row, true))
                cout<<"Digit: "<<predict(0,Row,Theta1,Theta2)<<endl;
            else
                if( exit.compare(input_name) != 0)
                    cout<<"File not found"<<endl;
            
            cout<<endl;
        }while( exit.compare(input_name) != 0);
    }
    else
    {
        cout<<"Reading data from:   "<<data_name<<endl;
        if(!readDatMatrix(data_name,X))
        {
            cout<<"File '"<<data_name<<"' not found"<<endl;
            return -1;
        }
        
        cout<<"Reading labels from: "<<labels_name<<endl<<endl;
        if(!readDatMatrix(labels_name,y))
        {
            cout<<"File '"<<labels_name<<"' not found"<<endl;
            return -1;
        }
        
        cout<<"Dimension of training set: "<<m<<endl;
        cout<<"Number of features:        "<<N<<endl<<endl;
        
        cout<<"Enter: "<<endl;
        cout<<"0 - random initialize weights"<<endl;
        cout<<"1 - initialize weights from Theta{1,2}.dat files\nImput: ";
        cin>>thetaInput;
        cout<<"You have choosen: "<<thetaInput<<endl<<endl;
        
        do
        {
            cout<<"Enter the number of iterations for gradient descent: ";
            cin>>iter;
            if(iter<=0)
                cout<<"Invalid input: the number of iteration must be positive."<<endl;
        }while(iter<=0);
        
        cout<<endl<<"Do you want to save the weights that gradient descent will find?"<<endl;
        cout<<"0 - no"<<endl;
        cout<<"1 - yes"<<endl<<"Input: ";
        cin>>saveOutput;
        cout<<"You have choosen: "<<saveOutput<<endl<<endl;
        
        //+++++++++++++++++++++++++++++ Allocation and initialization of weights and NEURH ++++++++++++++++
        // The following part is "obscure" if you don't know vectorized implementations of NN 

       
        if( thetaInput == false)
        {    
            //initialize weights randomly (breaking symmetry) 
            double eps1, eps2;
            eps1 = sqrt(6)/sqrt(N + NEURH);
            eps2 = sqrt(6)/sqrt(NEURH + KK);
            Theta1.randMatrix(-eps1, eps1);
            Theta2.randMatrix(-eps2, eps1);
        }
        else
        {
            string fname;
            fname = "Theta1.dat";

            if(!readDatMatrix(fname, Theta1))
            {
                cout<<"File '"<<fname<<"' not found"<<endl;
                return -1;
            }

            fname = "Theta2.dat";
            if(!readDatMatrix(fname, Theta2))
            {
                cout<<"File '"<<fname<<"' not found"<<endl;
                return -1;
            }
        }
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        #else
    {        
        cout<<"Reading data from:   "<<data_name<<endl;
        if(!readDatMatrix(data_name,X))
        {
            cout<<"File '"<<data_name<<"' not found"<<endl;
            return -1;
        }
        
        cout<<"Reading labels from: "<<labels_name<<endl<<endl;
        if(!readDatMatrix(labels_name,y))
        {
            cout<<"File '"<<labels_name<<"' not found"<<endl;
            return -1;
        }

        cout<<"iterations: "<<iter<<endl;
        
        if( thetaInput == false)
        {    
            cout<<"Initialize weights randomly"<<endl;
            //initialize weights randomly (breaking symmetry) 
            double eps1, eps2;
            eps1 = sqrt(6)/sqrt(N + NEURH);
            eps2 = sqrt(6)/sqrt(NEURH + KK);
            Theta1.randMatrix(-eps1, eps1);
            Theta2.randMatrix(-eps2, eps1);
        }
        else
        {
            cout<<"Reading Theta{1,2}.dat"<<endl;
            string fname;
            fname = "Theta1.dat";

            if(!readDatMatrix(fname, Theta1))
            {
                cout<<"File '"<<fname<<"' not found"<<endl;
                return -1;
            }

            fname = "Theta2.dat";
            if(!readDatMatrix(fname, Theta2))
            {
                cout<<"File '"<<fname<<"' not found"<<endl;
                return -1;
            }
        }
        #endif
        
        //initialize y_logic
        for(int i=0; i<m; i++)
            for(int j=0; j<KK; j++)
                if(y.elem[i][0] == j)
                    y_logic.elem[i][j] = 1;
                else
                    y_logic.elem[i][j] = 0;
        
        cout<<setprecision(10)<<"0  "<<batchCostFunction(m,X,y_logic,Theta1,Theta2,dTheta1,dTheta2)<<endl;
        GradDescent(m,X,y_logic,Theta1,Theta2,dTheta1,dTheta2,iter,true);

        if( saveOutput == true) 
        {
            string name_out;
            string theta;
            
            name_out = name_out.append("_alpha");
            name_out = name_out.append(to_string(g_alpha));
            name_out = name_out.append("_lambda");
            name_out = name_out.append(to_string((int)(g_lambda)));
            name_out = name_out.append(".dat");
            
            theta = "Theta1";
            saveMatrix(theta.append(name_out), Theta1);
            theta = "Theta2";
            saveMatrix(theta.append(name_out), Theta2);
        
        }

        Matrix P(m,1);

        predictAllX(m, X, Theta1, Theta2, P);

        //print accuracy
        double sum = 0; 

        for(int i=0; i<m; i++)
            if( abs(P.elem[i][0]-y.elem[i][0]) < 0.1 )
                sum++;
        
        cout<<"Accuracy on training set: "<<sum*100/m<<endl;
    
    }
    
    return 0;
}

void GradDescent(int m,Matrix X, Matrix y_logic, Matrix &Theta1, Matrix &Theta2, Matrix &dTheta1, Matrix &dTheta2, int
iterations, bool isBatch)
{
    
    for(int iter=0; iter<iterations; iter++)
    {
        for(int i=0; i<NEURH; i++)
            for(int j=0; j<N+1;j++)
                Theta1.elem[i][j] -= g_alpha*dTheta1.elem[i][j];

        for(int i=0; i<KK; i++)
            for(int j=0; j<NEURH+1;j++)
                Theta2.elem[i][j] -= g_alpha*dTheta2.elem[i][j];
        
        if( isBatch == true)
            cout<<iter+1<<"  "<<batchCostFunction(m,X, y_logic, Theta1, Theta2, dTheta1, dTheta2)<<endl;
        else
        {
            cout<<"Only Batch implemented"<<endl;
            break;
        }
    }

    return; 
}

double batchCostFunction(int m, Matrix &X, Matrix &y_logic, Matrix &Theta1, Matrix &Theta2, Matrix &dTheta1, Matrix &dTheta2)
{
    double J = 0, reg = 0;
    
    static double a1[N+1];
    static double z2[NEURH];
    static double a2[NEURH+1];
    static double a3[KK];
    static double delta2[NEURH+1];
    static double delta2_new[NEURH];
    static double sigGradz2[NEURH+1];
    double g;
    static double DELTA1[NEURH][N+1];
    static double DELTA2[KK][NEURH+1];
   

    for(int i=0; i<NEURH; i++)
        for(int j=0; j<N+1; j++)
            DELTA1[i][j] = 0;

    for(int i=0; i<KK; i++)
        for(int j=0; j<NEURH+1; j++)
            DELTA2[i][j] = 0;

    for(int t=0; t<m; t++)
    {
        a1[0] = 1;
        for(int i=1; i<N+1; i++) //a1 =[1 X(t,:)]
            a1[i] = X.elem[t][i-1];

        for(int i=0; i<NEURH; i++) //z2 = Theta1*a2
        {
            z2[i] = 0;
            for(int j=0; j<N+1; j++) 
                z2[i] += Theta1.elem[i][j]*a1[j];
        }
        
        a2[0] = 1; 
        for(int i=1; i<NEURH+1; i++) //a2 = [1 sigmoid(z2)]
            a2[i] = 1/(1+exp(-z2[i-1]));
        
        for(int i=0; i<KK; i++)  //a3 = z3 = Theta2*a2
        {
            a3[i] = 0; 
            for(int j=0; j<NEURH+1; j++)
                a3[i] += Theta2.elem[i][j]*a2[j];
        }
        
        for(int i=0; i<KK; i++) // a3 = sig(z3) = sig(a3)
            a3[i] = 1/(1+exp(-a3[i]));

        for(int k=0; k<KK; k++)
            J += -y_logic.elem[t][k]*log(a3[k])-(1-y_logic.elem[t][k])*log(1-a3[k]);
    
        //back prop
        for(int i=0; i<KK; i++) //a3 = delta3 = a3 - y_logic[t]
            a3[i] = a3[i] - y_logic.elem[t][i];
        

        for(int i=0; i<NEURH+1; i++)
        {
            delta2[i] = 0; 
            for(int j=0; j<KK; j++)
                delta2[i] += Theta2.elem[j][i]*a3[j];
        }
        
        sigGradz2[0] = 1;
        for(int i=1; i<NEURH+1; i++)
        {
            g = 1/(1+exp(-z2[i-1]));
            sigGradz2[i] = g*(1-g);
        }
        
        for(int i=1; i<NEURH+1; i++)
            delta2[i] = delta2[i]*sigGradz2[i];

        for(int i=0; i<NEURH; i++)
            delta2_new[i] = delta2[i+1];
        
        //matrix Theta1(NEURH, n+1), Theta2(K, NEURH+1);  // weights
        for(int i=0; i<NEURH; i++)
            for(int j=0; j<N+1; j++)
                DELTA1[i][j] += delta2_new[i]*a1[j];

        for(int i=0; i<KK; i++)
            for(int j=0; j<NEURH+1; j++)
                DELTA2[i][j] += a3[i]*a2[j];
    }

    for(int i=0; i<NEURH; i++)  //value of grad
        for(int j=0; j<N+1; j++)
            dTheta1.elem[i][j] = DELTA1[i][j]/m;

    for(int i=0; i<KK; i++)
        for(int j=0; j<NEURH+1; j++)
            dTheta2.elem[i][j] = DELTA2[i][j]/m;

    //reg of J
    for(int i=0; i<NEURH; i++)
        for(int j=0; j<=N; j++)
            reg += Theta1.elem[i][j]*Theta1.elem[i][j];
    for(int i=0; i<KK; i++)
        for(int j=0; j<=NEURH; j++)
            reg += Theta2.elem[i][j]*Theta2.elem[i][j];
    
    //reg of gradient:
    for(int i=0; i<NEURH; i++)
        for(int j=1; j<N+1;j++)
            dTheta1.elem[i][j] += g_lambda*Theta1.elem[i][j]/m;

    for(int i=0; i<KK; i++)
        for(int j=1; j<NEURH+1;j++)
            dTheta2.elem[i][j] += g_lambda*Theta2.elem[i][j]/m;
    
    return (J + reg*g_lambda/2)/m;    
}

void predictAllX(int m, Matrix X, Matrix Theta1, Matrix Theta2, Matrix &P)
{
    double max;
    int max_index;

    Matrix X1(m, N+1);
    Matrix h1(m, NEURH);
    Matrix h11(m, NEURH+1);
    Matrix h2(m, KK);

    if(!addColumn1(X,X1))
        cout<<"Error at step 1"<<endl;
    
    if(!productMatrixTransp2(X1,Theta1,h1))
        cout<<"Error at step 2"<<endl;

    if(!sigmoidMatrix(h1,h1))
        cout<<"Error at step 3"<<endl;

    if(!addColumn1(h1, h11))
        cout<<"Error at step 4"<<endl;

    if(!productMatrixTransp2(h11,Theta2, h2))
        cout<<"Error at step 5"<<endl;
   
    if(!sigmoidMatrix(h2,h2))
        cout<<"Error at step 6"<<endl;

    for(int i=0; i<m; i++)
    {
        max = h2.elem[i][0];
        max_index = 0;
        for(int j=1; j<KK; j++)
            if( h2.elem[i][j] > max)
            {
                max = h2.elem[i][j];
                max_index = j;
            }
       P.elem[i][0] = max_index;
    }
}
int predict(int row, Matrix X, Matrix Theta1, Matrix Theta2)
{
    static double a1[N+1];
    static double z2[NEURH];
    static double a2[NEURH+1];
    static double a3[KK];
    
    a1[0] = 1;
    for(int i=1; i<N+1; i++) //a1 =[1 X(t,:)]
        a1[i] = X.elem[row][i-1];

    for(int i=0; i<NEURH; i++) //z2 = Theta1*a2
    {
        z2[i] = 0;
        for(int j=0; j<N+1; j++) 
            z2[i] += Theta1.elem[i][j]*a1[j];
    }
    
    a2[0] = 1; 
    for(int i=1; i<NEURH+1; i++) //a2 = [1 sigmoid(z2)]
        a2[i] = 1/(1+exp(-z2[i-1]));
    
    for(int i=0; i<KK; i++)  //a3 = z3 = Theta2*a2
    {
        a3[i] = 0; 
        for(int j=0; j<NEURH+1; j++)
            a3[i] += Theta2.elem[i][j]*a2[j];
    }
    
    for(int i=0; i<KK; i++) // a3 = sig(z3) = sig(a3)
        a3[i] = 1/(1+exp(-a3[i]));

    int digit;
    double max;
    
    digit = 0;
    max = a3[0];
    for(int i=1; i<KK; i++)
    {
        if(a3[i] > max)
        {
            max = a3[i];
            digit = i; 
        }
    }
    
    return digit;
}
