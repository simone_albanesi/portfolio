#include <iostream>
#include <cmath>
#include <ctime>
#include <fstream>
#include <string>
#include <iomanip>

using namespace std;

#ifndef MATRIX_H
#define MATRIX_H

class Matrix 
{
public:
	Matrix(int,int);
    Matrix(const Matrix &); //copy constructor  
    int getRow();
    int getCol();
    void print();
    void randMatrix(double min, double max);
    bool copyArray2D(double**, int,int); //take 2D array and save in elem  
    
    ~Matrix();
    
    double **elem;

private:
	int row,col;  //dimensions (private because these must not be modified)
};

#endif

//+++++++++++++++++++++++++++++++++++++++ Imput and Output for pgm files +++++++++++++++++++++++++++++++++++
//read filename with M rows and N columns and save in matrix (dimMxN) 
bool readDat(string fname,double **matrix,int M, int N); 
bool readDatMatrix(string fname,Matrix &); 

// take the m-row of the matrix with N columns and make pgm with dimension dim x dim. If transpose is true,
// the image is the transpose of the matrix "reshaped" from the m-row. 
bool rowToPGM(string img_name, int dim, double **matrix, int m, int N, bool transpose);
bool PGMToRow(string img_name, int dim, Matrix &Row, bool transpose); //save pgm in row

void saveMatrix(string, Matrix);
//++++++++++++++++++++++++++++++++++++++++ Operations on matrix +++++++++++++++++++++++++++++++++++++++++++++
// IMPORTANT: these functions are useful because are 'clear', but you CAN'T use them in a cost-function, because that
// function would be too slow. Instead you can use it in functione like predict() in neuralNetwork.cpp 

//Every function return false if the dimensions of the matrices are wrong 
bool sumMatrix(Matrix A, Matrix B, Matrix &C); //save A+B in C  
bool productMatrixWithScalar(double,Matrix A, Matrix &C);//save s*A in C
bool productMatrix(Matrix A, Matrix B, Matrix &C);  //save A*B in C
bool expMatrix(Matrix, Matrix &); //apply exp() to each element of the matrix
bool logMatrix(Matrix, Matrix &); //apply log() to each element of the matrix
bool squareMatrix(Matrix, Matrix &); // square of each element of the matrix
bool sigmoidMatrix(Matrix, Matrix &); //1./(1+expMatrix(-Matrix)) 
bool sigmoidGradMatrix(Matrix, Matrix &); //1./(1+expMatrix(-Matrix)) 

//We can avoid a tranpose using some "special" product on matrices
bool productMatrixTransp1(Matrix, Matrix, Matrix &); // save A'*B in C (A' transpose of A)
bool productMatrixTransp2(Matrix, Matrix, Matrix &); // save A*B' in C (B' transpose of B)
bool addColumn1(Matrix, Matrix &); //copy A in B but with an additional column of 1

double sumElements(Matrix); //return the sum of all elemetns
bool elemProductMatrix(Matrix A, Matrix B, Matrix &C);






