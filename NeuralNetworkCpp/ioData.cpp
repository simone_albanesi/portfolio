#include "header.h"

//read filename with M rows and N columns and save in matrix (dimMxN)
bool readDat(string fname, double **matrix,int M, int N)  
{
    ifstream fdata;
    fdata.open(fname);
    
    if(fdata.fail())
        return false;
  
    for(int i=0; i<M; i++) //read
        for(int j=0; j<N; j++)
            fdata>>matrix[i][j];

    fdata.close();
    
    return true;
}
bool readDatMatrix(string fname, Matrix &A)  
{
    int M,N;
    M = A.getRow();
    N = A.getCol();
    ifstream fdata;
    fdata.open(fname);
    
    if(fdata.fail())
        return false;
  
    for(int i=0; i<M; i++) //read
        for(int j=0; j<N; j++)
            fdata>>A.elem[i][j];

    fdata.close();
    
    return true;
}
// take the m-row of the matrix with N columns and make pgm with dimension dim x dim. If transpose is true,
// the image is the transpose of the matrix "reshaped" from the m-row. 
bool rowToPGM(string img_name, int dim, double **matrix, int m, int N, bool transpose)
{
    double min,max;
    int max_pgm = 255;  
    double p;
    int **pgm_matrix;
    string magic("P2");
    ofstream img;
    
    pgm_matrix = new int*[dim];
    for (int i=0; i<dim; ++i)
        pgm_matrix[i] = new int[dim];

    img.open(img_name);
    img<<magic<<endl;
    img<<dim<<" "<<dim<<endl;
    img<<max_pgm<<endl;
    
    //first, we find max and min values of the array matrix[m]
    min = matrix[m][0];
    max = matrix[m][0];
    
    for(int i=1; i<N; i++)
    {
        p = matrix[m][i];
        if( p < min )
            min = p;
        if( p > max )
            max = p;
    }
    // now we save the row in a matrix-form
    // (we have to save it before write it because we need the transpose) 
    int i=0;
    int j=0;

    for(int k=0; k<N; k++)
    { 
        pgm_matrix[i][j]=(int)(max_pgm*((matrix[m][k]-min)/(max-min)));
        j++;

        if( (k+1)%dim == 0  )
        {
            i++;
            j=0;
        }
    }
   
    for(int i=0; i<dim; i++)
    {
        for(int j=0; j<dim; j++)
            if(transpose == true)
                img<<pgm_matrix[j][i]<<" ";
            else
                img<<pgm_matrix[i][j]<<" ";

        img<<endl;
    }
       
    for(int i=0; i<dim; i++)
        delete[] pgm_matrix[i];
    delete[] pgm_matrix;
       
    img.close();
    
    return true;
}

bool PGMToRow(string img_name, int dim, Matrix &Row, bool transpose)
{
    string magic;
    ifstream img;
    int M,N;
    int dim1, dim2;
    int max_pgm;

    M = Row.getRow();
    N = Row.getCol();

    if( M!=1 || N!=dim*dim )
        return false;

    img.open(img_name);

    if(img.fail())
        return false;
    
    img>>magic;
    img>>dim1>>dim2;
    img>>max_pgm;

    if(dim1!=dim2 || dim1!=dim)
    {
        img.close();
        return false;
    }
    
    Matrix A(dim,dim);

    for(int i=0; i<dim; i++)
        for(int j=0; j<dim; j++)
            if(transpose == true)
                img>>A.elem[j][i];
            else
                img>>A.elem[i][j];
    
    double min,max,mean; 
    double p, sum = 0;
    min = A.elem[0][0];
    max = A.elem[0][0];
    
    for(int i=0; i<dim; i++)
        for(int j=0; j<dim; j++)
            {
                p = A.elem[i][j];
                if( p < min )
                    min = p;
                if( p > max )
                    max = p;
                sum += p;
            }

    mean = sum/(dim*dim);        
        
    int k = 0; 
    for(int i=0; i<dim; i++)
        for(int j=0; j<dim; j++)
        { 
            Row.elem[0][k]= (A.elem[i][j] - mean)/(max-min);
            k++;
        }

    img.close();
    return true;
}
void saveMatrix(string fname, Matrix A)
{
    int M,N;
    ofstream fdata;

    fdata.open(fname);
    
    M = A.getRow();
    N = A.getCol();

    cout<<setiosflags(ios::scientific)<<fixed;
    
    for(int i=0; i<M; i++)
    {
        for(int j=0; j<N;j++)
            fdata<<A.elem[i][j]<<" ";
        fdata<<endl;
    }

    return; 
}
