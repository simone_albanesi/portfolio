#include "header.h"

//++++++++++++++++++++++++++++++++++++++++++++ Class Matrix +++++++++++++++++++++++++++++++++++++++++++++++++++++

Matrix::Matrix(int M, int N) //constructor
{
    row = M;
    col = N;
    elem = new double*[M];
    for(int i=0; i<M; i++)
        elem[i] = new double[N];
    
    //as convention, we initialize the matrix to ZeroMatrix
    for(int i=0; i<M; i++)
        for(int j=0; j<N; j++)
            elem[i][j] = 0;
}
Matrix::Matrix(const Matrix &mtr)
{
    row = mtr.row;
    col = mtr.col;
    
    elem = new double*[row];
    for(int i=0; i<row; i++)
        elem[i] = new double[col];
    
    //as convention, we initialize the matrix to ZeroMatrix
    for(int i=0; i<row; i++)
        for(int j=0; j<col; j++)
            elem[i][j] = mtr.elem[i][j];
}
int Matrix::getRow()
{
    return row;
}
int Matrix::getCol()
{
    return col;
}
void Matrix::print()
{
    cout<<setw(8)<<setprecision(3)<<fixed;
    //cout<<setw(15)<<setiosflags(ios::scientific)<<fixed;
    
    for(int i=0; i<row; i++)
    {
        for(int j=0; j<col; j++)
            cout<<setw(8)<<elem[i][j]<<" ";
        cout<<endl;
    }
    cout<<endl;

}
void Matrix::randMatrix(double min, double max)
{
    srand(time(NULL));

    for(int i=0; i<row; i++)
        for(int j=0; j<col; j++)
            elem[i][j] = (max-min)*rand()/RAND_MAX + min; 
    return;
}
bool Matrix::copyArray2D(double **mtr, int M, int N)
{
    if( M!=row || N!=col)
        return false;
    else
    {
        for(int i=0; i<M; i++)
            for(int j=0; j<N; j++)
                elem[i][j] = mtr[i][j];
        return true;
    }
}
Matrix::~Matrix() //destructor
{
    if(elem!=NULL) 
    {
        for(int i=0; i<row; i++)
            delete[] elem[i];
        delete[] elem;
        //cout<<"Deallocate Matrix.elem"<<endl;
    }
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++ functions that work on Matrix +++++++++++++++++++++++++++++++

bool sumMatrix(Matrix A, Matrix B, Matrix &C) //save A+B in C  
{
    int Ma,Na,Mb,Nb,Mc,Nc;
    
    Ma = A.getRow();
    Na = A.getCol();
    Mb = B.getRow();
    Nb = B.getCol();
    Mc = C.getRow();
    Nc = C.getCol();

    if( (Ma!=Mb) || (Mb!=Mc) || (Na!=Nb) || (Nb!=Nc) )
        return false;
    else
    {
        for(int i=0; i<Ma; i++)
            for(int j=0; j<Na; j++)
                C.elem[i][j] = A.elem[i][j]+B.elem[i][j];
        return true;
    }
}
bool productMatrixWithScalar(double s, Matrix A, Matrix &C) //save s*A in C
{
    int Ma,Na,Mc,Nc;
    
    Ma = A.getRow();
    Na = A.getCol();
    Mc = C.getRow();
    Nc = C.getCol();
    
    if( Ma!=Mc || Na!=Nc )
        return false;
    else
    {
        for(int i=0; i<Ma; i++)
            for(int j=0; j<Na; j++)
                C.elem[i][j] = s*A.elem[i][j];
        return true;
    }
}
bool productMatrix(Matrix A, Matrix B, Matrix &C)  //save A*B in C
{
    int Ma,Na,Mb,Nb,Mc,Nc;
    
    Ma = A.getRow();
    Na = A.getCol();
    Mb = B.getRow();
    Nb = B.getCol();
    Mc = C.getRow();
    Nc = C.getCol();

    if( Ma!=Mc || Nb!=Nc || Na!=Mb )
        return false;
    else
    {
        for(int i=0; i<Ma; i++)
            for(int j=0; j<Nb; j++)
            {
                C.elem[i][j] = 0;
                for(int k=0; k<Na; k++)
                    C.elem[i][j] += A.elem[i][k]*B.elem[k][j];  
            }
        return true;
    }
}
bool expMatrix(Matrix A, Matrix &C)
{
    int Ma,Na,Mc,Nc;
    
    Ma = A.getRow();
    Na = A.getCol();
    Mc = C.getRow();
    Nc = C.getCol();
    
    if( Ma!=Mc || Na!=Nc )
        return false;
    else
    {
        for(int i=0; i<Ma; i++)
            for(int j=0; j<Na; j++)
                C.elem[i][j] = exp(A.elem[i][j]);
        return true;
    }
}
bool logMatrix(Matrix A, Matrix &C)
{
    int Ma,Na,Mc,Nc;
    
    Ma = A.getRow();
    Na = A.getCol();
    Mc = C.getRow();
    Nc = C.getCol();
    
    if( Ma!=Mc || Na!=Nc )
        return false;
    else
    {
        for(int i=0; i<Ma; i++)
            for(int j=0; j<Na; j++)
                C.elem[i][j] = log(A.elem[i][j]);
        return true;
    }
}

bool sigmoidMatrix(Matrix A, Matrix &C)
{
    int Ma,Na,Mc,Nc;
    
    Ma = A.getRow();
    Na = A.getCol();
    Mc = C.getRow();
    Nc = C.getCol();
    
    if( Ma!=Mc || Na!=Nc )
        return false;
    else
    {
        for(int i=0; i<Ma; i++)
            for(int j=0; j<Na; j++)
                C.elem[i][j] = 1/(1+exp(-A.elem[i][j]));
        return true;
    }
}

bool sigmoidGradMatrix(Matrix A, Matrix &C) // g(1-g)
{
    int Ma,Na,Mc,Nc;
    double g;

    Ma = A.getRow();
    Na = A.getCol();
    Mc = C.getRow();
    Nc = C.getCol();
    
    if( Ma!=Mc || Na!=Nc )
        return false;
    else
    {
        for(int i=0; i<Ma; i++)
            for(int j=0; j<Na; j++)
            {
                g = 1/(1+exp(-A.elem[i][j]));
                C.elem[i][j] = g*(1-g);
            }
        return true;
    }
}


bool productMatrixTransp1(Matrix A, Matrix B, Matrix &C) // save A'*B in C (A' transpose of A)
{
    int Ma,Na,Mb,Nb,Mc,Nc;
    
    Ma = A.getRow();
    Na = A.getCol();
    Mb = B.getRow();
    Nb = B.getCol();
    Mc = C.getRow();
    Nc = C.getCol();
    
    if( Ma!=Mb || Mc!=Na || Nb!=Nc )
        return false;
    else
    {
        for(int i=0; i<Na; i++)
            for(int j=0; j<Nb; j++)
            {
                C.elem[i][j] = 0;
                for(int k=0; k<Ma; k++)
                    C.elem[i][j] += A.elem[k][i]*B.elem[k][j];
            }
            
        return true;
    }
}

bool productMatrixTransp2(Matrix A, Matrix B, Matrix &C) // save A*B' in C (B' transpose of B)
{
    int Ma,Na,Mb,Nb,Mc,Nc;
    
    Ma = A.getRow();
    Na = A.getCol();
    Mb = B.getRow();
    Nb = B.getCol();
    Mc = C.getRow();
    Nc = C.getCol();
    
    if( Na!=Nb || Ma!=Mc || Nc!=Mb )
        return false;
    else
    {
        for(int i=0; i<Ma; i++)
            for(int j=0; j<Mb; j++)
            {
                C.elem[i][j] = 0;
                for(int k=0; k<Na; k++)
                    C.elem[i][j] += A.elem[i][k]*B.elem[j][k];
            }
            
        return true;
    }
}

bool addColumn1(Matrix A, Matrix &C) //copy A in B but with an additional column of 1
{
    int Ma,Na,Mc,Nc;

    Ma = A.getRow();
    Na = A.getCol();
    Mc = C.getRow();
    Nc = C.getCol();
        
    if( Ma!=Mc || (Na+1)!=Nc )
        return false;
    else
    {
        for(int i=0; i<Ma; i++)
            for(int j=0; j<Na; j++)
                C.elem[i][j+1] = A.elem[i][j];
        for(int i=0; i<Ma; i++)
            C.elem[i][0] = 1;
        return true;
    }
}

double sumElements(Matrix A) //return the sum of all elemetns
{
    double sum = 0;
    int M,N;
    M = A.getRow();
    N = A.getCol();

    for(int i=0; i<M; i++)
        for(int j=0; j<N; j++)
            sum += A.elem[i][j];
    
    return sum;
}

bool elemProductMatrix(Matrix A, Matrix B, Matrix &C)
{
    int Ma,Na,Mb,Nb,Mc,Nc;
    
    Ma = A.getRow();
    Na = A.getCol();
    Mb = B.getRow();
    Nb = B.getCol();
    Mc = C.getRow();
    Nc = C.getCol();
    
    if( Na!=Nb || Ma!=Mb || Nb!=Nc || Ma!=Mc )
        return false;
    else
    {
        for(int i=0; i<Ma; i++)
            for(int j=0; j<Na; j++)
                C.elem[i][j] = A.elem[i][j]*B.elem[i][j];
            
        return true;
    }
}




