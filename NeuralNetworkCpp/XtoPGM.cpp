#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <iomanip>
#include <ctime>
#include "header.h"
#define NEURH 25   // number of neuron in the hidden layer
#define N 400       // number of features (i.e. 400 for 20x20 pixels images)
#define KK 10       // number of labels

using namespace std;

int main()
{
    int m =5000;
    string fname = "X.dat";
    ifstream fdata;
    string imgname = "img";
    Matrix X(m,N);
    
    fdata.open(fname);

    if(fdata.fail())
    {
        cout<<"File '"<<fname<<"' not found"<<endl;
        return -1;
    }
    
    for(int i=0; i<m; i++)
        for(int j=0; j<N; j++)
            fdata>>X.elem[i][j];

    for(int t=0; t<m; t++)
    {
        imgname = "img";
        imgname = imgname.append(to_string( (int) (t+1)));
        imgname = imgname.append(".pgm");
        rowToPGM(imgname, 20, X.elem, t, N, true);
    }
    
    return 0;
}
